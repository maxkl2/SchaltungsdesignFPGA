
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity top is
    generic (
        N_LEDS : natural := 18
    );

    port (
        clk : in std_logic;
        rst : in std_logic;

        buttons : in std_logic_vector(3 downto 0);
        dipswitches : in std_logic_vector(N_LEDS-1 downto 0);
        
        leds : out std_logic_vector(N_LEDS-1 downto 0);
        segment_display_0 : out std_logic_vector(6 downto 0);
        segment_display_1 : out std_logic_vector(6 downto 0);
        segment_display_2 : out std_logic_vector(6 downto 0);
        segment_display_3 : out std_logic_vector(6 downto 0)
    );
end entity;

architecture behavioural of top is

    signal buttons_debounced : std_logic_vector(3 downto 0);
    signal run : std_logic;
    signal reset_pos : std_logic;
    signal latch_enable : std_logic;
    signal speed : unsigned(3 downto 0);
    signal clk_en : std_logic;
    signal cycle_counter : unsigned(15 downto 0);

begin

    -- Buttons are active low but controller expects active high
    -- TODO: add debouncer
    buttons_debounced <= not buttons;

    controller : entity work.controller
        port map (
            clk => clk,
            rst => rst,
            buttons => buttons_debounced,

            o_run => run,
            reset_pos => reset_pos,
            latch_enable => latch_enable,
            speed => speed
        );

    clock_divider : entity work.clock_divider
        generic map (
            F_CLK => 50_000_000, -- Hz
            F_SLOWEST => 1 -- Hz
        )
        port map (
            clk => clk,
            rst => rst,
            speed => speed,

            clk_en => clk_en
        );

    lauflicht : entity work.lauflicht
        generic map (
            N_LEDS => 18
        )
        port map (
            clk => clk,
            rst => rst,
            pattern => dipswitches,
            clk_en => clk_en,
            run => run,
            reset_pos => reset_pos,
            latch_enable => latch_enable,

            leds => leds,
            cycle_counter => cycle_counter
        );

    seg_enc_0 : entity work.segment_encoder
        port map (
            binary => std_logic_vector(cycle_counter(3 downto 0)),
            segments => segment_display_0
        );
    seg_enc_1 : entity work.segment_encoder
        port map (
            binary => std_logic_vector(cycle_counter(7 downto 4)),
            segments => segment_display_1
        );
    seg_enc_2 : entity work.segment_encoder
        port map (
            binary => std_logic_vector(cycle_counter(11 downto 8)),
            segments => segment_display_2
        );
    seg_enc_3 : entity work.segment_encoder
        port map (
            binary => std_logic_vector(cycle_counter(15 downto 12)),
            segments => segment_display_3
        );

end behavioural;