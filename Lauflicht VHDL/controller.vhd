
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity controller is
    port (
        clk : in std_logic;
        rst : in std_logic;
        buttons : in std_logic_vector(3 downto 0);

        o_run : out std_logic;
        reset_pos : out std_logic;
        latch_enable : out std_logic;
        speed : out unsigned(3 downto 0)
    );
end entity;

architecture behavioural of controller is

    type state_type is (LATCH, RESET, PAUSED, RUN);

    signal state : state_type;

    signal prev_buttons : std_logic_vector(3 downto 0);

begin

    process (clk)
        variable button_edges : std_logic_vector(3 downto 0);
    begin
        if rising_edge(clk) then
            if rst = '1' then
                speed <= to_unsigned(1, speed'length);

                state <= LATCH;
                prev_buttons <= (others => '0');
            else
                button_edges := buttons and not prev_buttons;
                prev_buttons <= buttons;

                case state is
                    when LATCH =>
                        if button_edges(0) = '1' then
                            state <= RUN;
                        end if;
                    when RESET =>
                        if button_edges(0) = '1' then
                            state <= RUN;
                        elsif button_edges(1) = '1' then
                            state <= LATCH;
                        end if;
                    when PAUSED =>
                        if button_edges(0) = '1' then
                            state <= RUN;
                        elsif button_edges(1) = '1' then
                            state <= RESET;
                        end if;
                    when RUN =>
                        if button_edges(0) = '1' then
                            state <= PAUSED;
                        elsif button_edges(1) = '1' then
                            state <= RESET;
                        end if;
                end case;

                if button_edges(2) = '1' then
                    if speed > 1 then
                        speed <= speed - 1;
                    end if;
                elsif button_edges(3) = '1' then
                    if speed < 10 then
                        speed <= speed + 1;
                    end if;
                end if;
            end if;
        end if;
    end process;

    process (state) begin
        case state is
            when LATCH =>
                o_run <= '0';
                reset_pos <= '1';
                latch_enable <= '1';
            when RESET =>
                o_run <= '0';
                reset_pos <= '1';
                latch_enable <= '0';
            when PAUSED =>
                o_run <= '0';
                reset_pos <= '0';
                latch_enable <= '0';
            when RUN =>
                o_run <= '1';
                reset_pos <= '0';
                latch_enable <= '0';
            when others =>
                o_run <= '0';
                reset_pos <= '0';
                latch_enable <= '0';
        end case;
    end process;

end behavioural;