
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;

use std.env.stop;

entity tb_lauflicht is

end entity;

architecture simulation of tb_lauflicht is

    constant N_LEDS : natural := 8;

    -- input
    signal clk : std_logic := '0';
    signal rst : std_logic;
    signal pattern : std_logic_vector(N_LEDS-1 downto 0);
    signal clk_en : std_logic := '0';
    signal run : std_logic;
    signal reset_pos : std_logic;
    signal latch_enable : std_logic;

    -- output
    signal leds : std_logic_vector(N_LEDS-1 downto 0);
    signal cycle_counter : unsigned(15 downto 0);

begin
    clk <= not clk after 10 ns; -- 50 MHz
    rst <= '1', '0' after 100 ns;

    clk_en <= not clk_en when rising_edge(clk);

    lauflicht : entity work.lauflicht
        generic map (
            N_LEDS => N_LEDS
        )
        port map (
            clk => clk,
            rst => rst,
            pattern => pattern,
            clk_en => clk_en,
            run => run,
            reset_pos => reset_pos,
            latch_enable => latch_enable,

            leds => leds,
            cycle_counter => cycle_counter
        );

    process begin
        latch_enable <= '0';
        reset_pos <= '0';
        run <= '0';

        wait until falling_edge(rst);

        report "out of reset";

        wait until rising_edge(clk);

        pattern <= "00000001";

        wait until rising_edge(clk);

        latch_enable <= '1';
        reset_pos <= '1';

        wait until rising_edge(clk);

        pattern <= "00010001";

        wait until rising_edge(clk);
        wait until rising_edge(clk);

        latch_enable <= '0';
        reset_pos <= '0';

        wait until rising_edge(clk);

        run <= '1';

        wait until rising_edge(clk);

        wait for 5 us;

        wait until rising_edge(clk);

        reset_pos <= '1';

        wait until rising_edge(clk);
        wait until rising_edge(clk);
        wait until rising_edge(clk);

        run <= '0';

        wait until rising_edge(clk);

        reset_pos <= '0';

        wait until rising_edge(clk);

        run <= '1';

        wait for 5 us;

        wait until rising_edge(clk);

        pattern <= "00001111";

        wait until rising_edge(clk);
        wait until rising_edge(clk);
        wait until rising_edge(clk);

        run <= '0';

        wait until rising_edge(clk);
        wait until rising_edge(clk);

        latch_enable <= '1';

        wait until rising_edge(clk);
        
        reset_pos <= '1';

        wait until rising_edge(clk);

        latch_enable <= '0';

        wait until rising_edge(clk);

        reset_pos <= '0';
        run <= '1';

        wait until rising_edge(clk);

        wait for 5 us;

        wait until rising_edge(clk);

        stop;
    end process;

end simulation;