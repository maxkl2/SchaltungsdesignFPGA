
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;

use std.env.stop;

entity tb_controller is

end entity;

architecture simulation of tb_controller is

    -- input
    signal clk : std_logic := '0';
    signal rst : std_logic;
    signal btn_run : std_logic := '0';
    signal btn_rst : std_logic := '0';
    signal btn_slower : std_logic := '0';
    signal btn_faster : std_logic := '0';
    --signal buttons : std_logic_vector(3 downto 0);

    -- output
    signal run : std_logic;
    signal reset_pos : std_logic;
    signal latch_enable : std_logic;
    signal speed : unsigned(3 downto 0);

    signal outbits : std_logic_vector(2 downto 0);

    procedure click (
        signal btn : out std_logic
    ) is begin
        wait until falling_edge(clk);
        btn <= '1';
        wait until rising_edge(clk);
        btn <= '0';
        wait until falling_edge(clk);
    end click;

begin
    clk <= not clk after 10 ns; -- 50 MHz
    rst <= '1', '0' after 100 ns;

    controller : entity work.controller
        port map (
            clk => clk,
            rst => rst,
            buttons(0) => btn_run,
            buttons(1) => btn_rst,
            buttons(2) => btn_slower,
            buttons(3) => btn_faster,

            o_run => run,
            reset_pos => reset_pos,
            latch_enable => latch_enable,
            speed => speed
        );

    outbits <= latch_enable & reset_pos & run;

    process begin
        wait until falling_edge(rst);

        report "out of reset";

        wait until rising_edge(clk);

        -- Initially in Latch
        wait until falling_edge(clk);
        assert outbits = "110" report "outbits = " & to_string(outbits);

        -- To Latch
        click(btn_rst);
        assert outbits = "110" report "outbits = " & to_string(outbits);

        -- To Run
        click(btn_run);
        assert outbits = "001" report "outbits = " & to_string(outbits);

        -- To Paused
        click(btn_run);
        assert outbits = "000" report "outbits = " & to_string(outbits);

        -- To Reset
        click(btn_rst);
        assert outbits = "010" report "outbits = " & to_string(outbits);

        -- To Run
        click(btn_run);
        assert outbits = "001" report "outbits = " & to_string(outbits);

        -- To Paused
        click(btn_run);
        assert outbits = "000" report "outbits = " & to_string(outbits);

        -- To Run
        click(btn_run);
        assert outbits = "001" report "outbits = " & to_string(outbits);

        -- To Reset
        click(btn_rst);
        assert outbits = "010" report "outbits = " & to_string(outbits);

        -- To Latch
        click(btn_rst);
        assert outbits = "110" report "outbits = " & to_string(outbits);

        report "All transitions good";

        assert speed = 1;

        click(btn_faster);
        assert speed = 2;
        click(btn_faster);
        assert speed = 3;
        click(btn_faster);
        assert speed = 4;
        click(btn_faster);
        assert speed = 5;
        click(btn_faster);
        assert speed = 6;
        click(btn_faster);
        assert speed = 7;
        click(btn_faster);
        assert speed = 8;
        click(btn_faster);
        assert speed = 9;
        click(btn_faster);
        assert speed = 10;
        click(btn_faster);
        assert speed = 10;
        click(btn_slower);
        assert speed = 9;
        click(btn_slower);
        assert speed = 8;
        click(btn_slower);
        assert speed = 7;
        click(btn_slower);
        assert speed = 6;
        click(btn_slower);
        assert speed = 5;
        click(btn_slower);
        assert speed = 4;
        click(btn_slower);
        assert speed = 3;
        click(btn_slower);
        assert speed = 2;
        click(btn_slower);
        assert speed = 1;
        click(btn_slower);
        assert speed = 1;

        stop;
    end process;

end simulation;