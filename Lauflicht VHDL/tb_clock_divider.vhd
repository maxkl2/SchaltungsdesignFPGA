
library ieee;
use ieee.std_logic_1164.all;
use ieee.math_real.all;
use ieee.numeric_std.all;

use std.env.stop;

entity tb_clock_divider is

end entity;

architecture simulation of tb_clock_divider is

    -- input
    signal clk : std_logic := '0';
    signal rst : std_logic;
    signal speed : unsigned(3 downto 0) := "0001";

    -- output
    signal clk_en : std_logic;

begin
    clk <= not clk after 10 ns; -- 50 MHz
    rst <= '1', '0' after 100 ns;

    clock_divider : entity work.clock_divider
        generic map (
            F_CLK => 50_000_000, -- Hz
            F_SLOWEST => 1 -- Hz
        )
        port map (
            clk => clk,
            rst => rst,
            speed => speed,

            clk_en => clk_en
        );

    process
        variable t : time := 0 ns;
        variable dt : time := 0 ns;

        variable i : natural := 0;
    begin
        speed <= "0001";

        wait until falling_edge(rst);

        report "out of reset";

        t := now;

        wait until rising_edge(clk_en);
        dt := now - t;
        t := now;
        report "rising edge = +" & integer'image(dt / 1 ns) & " ns";
        wait until falling_edge(clk_en);
        dt := now - t;
        report "rising edge = +" & integer'image(dt / 1 ns) & " ns";

        t := now;

        report "setting speed to 10";

        speed <= "1010";

        wait until rising_edge(clk_en);
        dt := now - t;
        t := now;
        report "rising edge = +" & integer'image(dt / 1 ns) & " ns";
        wait until falling_edge(clk_en);
        dt := now - t;
        report "rising edge = +" & integer'image(dt / 1 ns) & " ns";

        stop;
    end process;

end simulation;
