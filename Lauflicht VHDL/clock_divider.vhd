
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;

entity clock_divider is
    generic (
        F_CLK : natural;
        F_SLOWEST : natural
    );

    port (
        clk : in std_logic;
        rst : in std_logic;
        speed : in unsigned(3 downto 0);

        clk_en : out std_logic
    );
end entity;

architecture behavioural of clock_divider is

    constant COUNTER_MAX : natural := F_CLK / F_SLOWEST;
    constant COUNTER_LEN : natural := natural(ceil(log2(real(COUNTER_MAX))));

    signal counter : unsigned(COUNTER_LEN downto 0);

begin

    process (clk) begin
        if rising_edge(clk) then
            if rst = '1' then
                counter <= (others => '0');
                clk_en <= '0';
            else
                if counter < COUNTER_MAX then
                    counter <= counter + unsigned(speed);
                    clk_en <= '0';
                else
                    counter <= (others => '0');
                    clk_en <= '1';
                end if;
            end if;
        end if;
    end process;

end behavioural;