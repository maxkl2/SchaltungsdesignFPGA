
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;

use std.env.stop;

entity tb_bandsperre is

end entity;

architecture simulation of tb_bandsperre is

    -- input
    signal clk : std_logic := '0';
    signal rst : std_logic;
    signal x : signed(13 downto 0);

    -- output
    signal y : signed(13 downto 0);

begin
    clk <= not clk after 10 ns; -- 50 MHz
    rst <= '1', '0' after 100 ns;

    bandsperre : entity work.bandsperre
        port map (
            clk => clk,
            rst => rst,
            x => x,
            y => y
        );

    process begin
        x <= 14x"0000";

        wait until falling_edge(rst);

        report "out of reset";

        wait until rising_edge(clk);

        x <= 14x"1000";

        wait until rising_edge(clk);

        x <= 14x"0000";

        wait until rising_edge(clk);

        wait until rising_edge(clk);

        wait until rising_edge(clk);

        wait until rising_edge(clk);

        wait until rising_edge(clk);

        wait until rising_edge(clk);

        stop;
    end process;

end simulation;