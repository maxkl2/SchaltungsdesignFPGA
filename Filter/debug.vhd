
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity debug is
    port (
        clk : in std_logic;
        rst : in std_logic;
        x : in signed(13 downto 0);

        y : out signed(13 downto 0)
    );
end entity;

architecture behavioural of debug is
begin

	process (clk)
	begin
		if rising_edge(clk) then
			if rst = '1' then
				y <= (others => '0');
			else
				-- Just to check whether ADC and DAC work and use 2s complement
				y <= -x;
			end if;
		end if;
	end process;

end behavioural;
