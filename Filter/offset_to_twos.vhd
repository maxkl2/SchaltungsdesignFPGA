
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity offset_to_twos is
    port (
        x : in unsigned(13 downto 0);

        y : out signed(13 downto 0)
    );
end entity;

architecture behavioural of offset_to_twos is
begin

	-- Invert sign bit to convert offset binary to two's complement
	y <= signed(x xor resize(x"2000", 14));

end behavioural;
