onerror {quit -f}
vlib work
vlog -work work Filter.vo
vlog -work work Filter.vt
vsim -novopt -c -t 1ps -L cycloneive_ver -L altera_ver -L altera_mf_ver -L 220model_ver -L sgate work.Hochpass_vlg_vec_tst
vcd file -direction Filter.msim.vcd
vcd add -internal Hochpass_vlg_vec_tst/*
vcd add -internal Hochpass_vlg_vec_tst/i1/*
add wave /*
run -all
