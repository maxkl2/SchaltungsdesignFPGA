
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity downsampler is
    port (
        clk_fast : in std_logic;
		  clk_slow : in std_logic;
        data_fast : in std_logic_vector(13 downto 0);

        data_slow : out std_logic_vector(13 downto 0)
    );
end entity;

architecture behavioural of downsampler is
	signal reg : std_logic_vector(13 downto 0);
begin

	process (clk_fast)
	begin
		if rising_edge(clk_fast) then
			reg <= data_fast;
		end if;
	end process;

	process (clk_slow)
	begin
		if rising_edge(clk_slow) then
			data_slow <= reg;
		end if;
	end process;

end behavioural;
