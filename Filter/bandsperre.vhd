
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity bandsperre is
	generic (
		COEFF_0 : signed(13 downto 0) := resize(x"1000", 14); -- 0.5
		COEFF_1 : signed(13 downto 0) := resize(x"0000", 14); -- 0.0
		COEFF_2 : signed(13 downto 0) := resize(x"1000", 14)  -- 0.5
	);
    port (
        clk : in std_logic;
        rst : in std_logic;
        x : in signed(13 downto 0);

        y : out signed(13 downto 0)
    );
end entity;

architecture behavioural of bandsperre is
	signal mult_0, mult_1, mult_2 : signed(13 downto 0);
	signal sum_0, sum_1 : signed(13 downto 0);
begin

	process (clk)
	begin
		if rising_edge(clk) then
			if rst = '1' then
				y <= (others => '0');

				mult_0 <= (others => '0');
				mult_1 <= (others => '0');
				mult_2 <= (others => '0');
				sum_0 <= (others => '0');
				sum_1 <= (others => '0');
			else
				y <= sum_1 + mult_2;
				sum_1 <= sum_0 + mult_1;
				sum_0 <= mult_0;

				-- Multiplier pipeline stage
				-- multiplication of 2 x Q1.13 results in Q2.26
				-- slice converts Q2.26 to Q1.13
				mult_0 <= "*"(x, COEFF_0)(26 downto 13);
				mult_1 <= "*"(x, COEFF_1)(26 downto 13);
				mult_2 <= "*"(x, COEFF_2)(26 downto 13);
			end if;
		end if;
	end process;

end behavioural;
