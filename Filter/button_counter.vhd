
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity button_counter is
    port (
        clk : in std_logic;
        rst : in std_logic;
        button : in std_logic;

        o_count : out unsigned(1 downto 0)
    );
end entity;

architecture behavioural of button_counter is

    signal prev_button : std_logic;
	 
	 signal count : unsigned(1 downto 0);

begin

	o_count <= count;

    process (clk)
        variable button_edge : std_logic;
    begin
        if rising_edge(clk) then
            if rst = '1' then
                count <= (others => '0');

                prev_button <= '0';
            else
                button_edge := button and not prev_button;
                prev_button <= button;

                if button_edge = '1' then
                    count <= count + 1;
                end if;
            end if;
        end if;
    end process;

end behavioural;