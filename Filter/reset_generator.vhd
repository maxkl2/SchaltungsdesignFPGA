
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity reset_generator is
    port (
        clk : in std_logic;

        o_rst : out std_logic
    );
end entity;

architecture behavioural of reset_generator is
	signal rst_counter : unsigned(9 downto 0) := (others => '1');
	signal rst : std_logic := '1';
begin

	o_rst <= rst;

	process (clk) begin
		if rising_edge(clk) then
			if rst_counter > 0 then
				rst_counter <= rst_counter - 1;
				rst <= '1';
			else
				rst <= '0';
			end if;
		end if;
	end process;

end behavioural;