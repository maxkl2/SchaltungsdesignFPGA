
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity twos_to_offset is
    port (
        x : in signed(13 downto 0);

        y : out unsigned(13 downto 0)
    );
end entity;

architecture behavioural of twos_to_offset is
begin

	-- Invert sign bit to convert two's complement to offset binary
	y <= unsigned(-x) xor resize(x"2000", 14);

end behavioural;
