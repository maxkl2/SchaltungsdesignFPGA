
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity clk_div is
    port (
        clk_in : in std_logic;

        clk_out : out std_logic
    );
end entity;

architecture behavioural of clk_div is
	signal counter : unsigned(3 downto 0) := (others => '0');
begin

	clk_out <= counter(3);

	process (clk_in)
	begin
		if rising_edge(clk_in) then
			counter <= counter + 1;
		end if;
	end process;

end behavioural;
