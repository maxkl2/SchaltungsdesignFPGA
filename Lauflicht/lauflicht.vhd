
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;

entity lauflicht is
    generic (
        N_LEDS : natural
    );

    port (
        clk : in std_logic;
        rst : in std_logic;
        pattern : in std_logic_vector(N_LEDS-1 downto 0);
        clk_en : in std_logic;
        run : in std_logic;
        reset_pos : in std_logic;
        latch_enable : in std_logic;

        o_leds : out std_logic_vector(N_LEDS-1 downto 0);
        o_cycle_counter : out unsigned(15 downto 0)
    );
end entity;

architecture behavioural of lauflicht is
    constant SHIFT_COUNTER_LEN : natural := natural(ceil(log2(real(N_LEDS))));

    signal pattern_latched : std_logic_vector(N_LEDS-1 downto 0);

    signal shift_counter : unsigned(SHIFT_COUNTER_LEN downto 0);
    signal down : std_logic;
	 
	 signal leds : std_logic_vector(N_LEDS-1 downto 0);
	 
	 signal cycle_counter : unsigned(15 downto 0);
begin

	o_leds <= leds;
	o_cycle_counter <= cycle_counter;

    process (clk) begin
        if rising_edge(clk) then
            if rst = '1' then
                leds <= (others => '0');
                cycle_counter <= (others => '0');

                pattern_latched <= (others => '0');
                shift_counter <= (others => '0');
                down <= '0';
            else
                if latch_enable = '1' then
                    pattern_latched <= pattern;
                end if;

                if reset_pos = '1' then
                    leds <= pattern_latched;

                    shift_counter <= (others => '0');
                    down <= '0';
                    
                    cycle_counter <= (others => '0');
                elsif run = '1' then
                    if clk_en = '1' then
                        if down = '1' then
                            leds <= std_logic_vector(rotate_right(unsigned(leds), 1));
                        else
                            leds <= std_logic_vector(rotate_left(unsigned(leds), 1));
                        end if;

                        if shift_counter < N_LEDS-1 then
                            shift_counter <= shift_counter + 1;
                        else
                            shift_counter <= (others => '0');
                            down <= not down;
                            
                            if down = '1' then
                                cycle_counter <= cycle_counter + 1;
                            end if;
                        end if;
                    end if;
                end if;
            end if;
        end if;
    end process;

end behavioural;