
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity top_quartus is
    port (
        CLOCK_50 : in std_logic;

        KEY : in std_logic_vector(3 downto 0);
        SW : in std_logic_vector(17 downto 0);
        
        LEDR : out std_logic_vector(17 downto 0);
        HEX0 : out std_logic_vector(6 downto 0);
        HEX1 : out std_logic_vector(6 downto 0);
        HEX2 : out std_logic_vector(6 downto 0);
        HEX3 : out std_logic_vector(6 downto 0)
    );
end entity;

architecture behavioural of top_quartus is
	signal rst_counter : unsigned(9 downto 0) := (others => '1');
	signal rst : std_logic := '1';
begin

	process (CLOCK_50) begin
		if rising_edge(CLOCK_50) then
			if rst_counter > 0 then
				rst_counter <= rst_counter - 1;
				rst <= '1';
			else
				rst <= '0';
			end if;
		end if;
	end process;

    top : entity work.top
		 generic map (
			  N_LEDS => 18
		 )
		 port map (
			  clk => CLOCK_50,
			  rst => rst,

			  buttons => (0 => KEY(3), 1 => KEY(2), 2 => KEY(1), 3 => KEY(0)),
			  dipswitches => SW,
			  
			  leds => LEDR,
			  segment_display_0 => HEX0,
			  segment_display_1 => HEX1,
			  segment_display_2 => HEX2,
			  segment_display_3 => HEX3
		 );

end behavioural;