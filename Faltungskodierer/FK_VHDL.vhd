
library ieee;
use ieee.std_logic_1164.all;

entity FK_VHDL is
	port(
		clk : in	std_logic;
		rst : in	std_logic;
		x : in std_logic;
		y : out std_logic_vector(1 downto 0)
	);
end entity;

architecture behavioural of FK_VHDL is

	type state_type is (s00, s01, s10, s11);

	signal state : state_type;

begin

	process (clk, rst) begin
		if rst = '1' then
			state <= s00;
			y <= "00";
		elsif (rising_edge(clk)) then
			case state is
				when s00 =>
					if x = '1' then
						state <= s01;
						y <= "11";
					else
						state <= s00;
						y <= "00";
					end if;
				when s01 =>
					if x = '1' then
						state <= s11;
						y <= "01";
					else
						state <= s10;
						y <= "10";
					end if;
				when s10 =>
					if x = '1' then
						state <= s01;
						y <= "00";
					else
						state <= s00;
						y <= "11";
					end if;
				when s11 =>
					if x = '1' then
						state <= s11;
						y <= "10";
					else
						state <= s10;
						y <= "01";
					end if;
			end case;
		end if;
	end process;

end behavioural;
