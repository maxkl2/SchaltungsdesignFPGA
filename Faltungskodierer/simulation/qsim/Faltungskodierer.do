onerror {quit -f}
vlib work
vlog -work work Faltungskodierer.vo
vlog -work work Faltungskodierer.vt
vsim -novopt -c -t 1ps -L cycloneii_ver -L altera_ver -L altera_mf_ver -L 220model_ver -L sgate work.Faltungskodierer_vlg_vec_tst
vcd file -direction Faltungskodierer.msim.vcd
vcd add -internal Faltungskodierer_vlg_vec_tst/*
vcd add -internal Faltungskodierer_vlg_vec_tst/i1/*
add wave /*
run -all
